
public class Box implements Pict{
	private double height;
	private double width;
	private final char char_edge;
	private final char char_content;
	
	//Keine Setter da nicht veraenderbar
	public double getHeight() {
		return height;
	}

	public double getWidth() {
		return width;
	}
//Fuer normale Box
	public Box(double height, double width, char char_edge, char char_content){
		this.height = height;
		this.width = width;
		this.char_edge = char_edge;
		this.char_content = char_content;
	}
//Fuer ClearBox
	public Box(double height, double width){
		this.height = height;
		this.width = width;
		this.char_edge = '*';
		this.char_content = ' ';
	}
//Fuer DarkBox
	public Box(double height, double width, char char_both){
		this.height = height;
		this.width = width;
		this.char_edge = char_both;
		this.char_content = char_both;
	}
	
	
	@Override
	public void scale(double factor) {
		height = height * factor;
		width *= factor;
		
	}
	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub

		String ret = "";
		double iheight = Math.ceil(height);
		double iwidth = Math.ceil(width);
		
		
		for (int i = 0; i < iheight; i++) {
			for(int j = 0; j < iwidth; j++){
				//Ueberpruefung fuer den Rand
				if(i == 0 || i == iheight-1 || j == 0 || j == iwidth-1)
					ret += char_edge;
				else
					ret += char_content;
			}
			ret += "\n";
		}
		
		return ret;
	}

}

