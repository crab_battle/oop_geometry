package oop9;

/*
 * eine Position ist ein Teil einer Bestellung
 */
public class Position {
	private int anzahl;	// > 0
	private String form;	// r, m oder w
	private String teig;	// m, z oder s
	private String fuellung; // Schokolade, Marmelade oder null (falls kein Doppelkeks bestellt wurde)
	
	// Konstruktor fuer Einzelkekse
	public Position(int anzahl, String form, String teig) {
		this.anzahl = anzahl;
		this.form = form;
		this.teig = teig;
	}
	
	// Konstruktor fuer Doppelkekse
	public Position(int anzahl, String form, String teig, String fuellung)
	{
		this.anzahl = anzahl;
		this.form = form;
		this.teig = teig;
		this.fuellung = fuellung;
	}
	
	public int getAnzahl() {
		return anzahl;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	public String getForm() {
		return form;
	}
	public void setForm(String form) {
		this.form = form;
	}
	public String getTeig() {
		return teig;
	}
	public void setTeig(String teig) {
		this.teig = teig;
	}
	public String getFuellung() {
		return fuellung;
	}
	public void setFuellung(String fuellung) {
		this.fuellung = fuellung;
	}
	
	// wandelt die codierte Form in ein Wort um, form = r,m oder w
	public String formToString() {
		if(form.equalsIgnoreCase("r")) {
			return "rund";
		} else if (form.equalsIgnoreCase("m")) {
			return "Mondform";
		} else if (form.equalsIgnoreCase("w")) {
			return "Weihnachtsmann";
		} else {
			return null;	// sollte nicht passieren
		}
	}
	
	// wandelt den codierten Teig in ein Wort um, teig = m,z oder s
	public String teigToString() {
		if(teig.equalsIgnoreCase("m")) {
			return "Muerbteig";
		} else if (teig.equalsIgnoreCase("z")) {
			return "Zimtsternteig";
		} else if (teig.equalsIgnoreCase("s")) {
			return "Schokoladenteig";
		} else {
			return null;	// sollte nicht passieren
		}
	}
}
