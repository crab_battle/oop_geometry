package oop9;

public abstract class Keksmaschine {
	

	// erzeugt einen Keks (keinen Doppelkeks)
	public Keks backe(String teig)
	{
		Keks k = null;
		if(this instanceof Keksmaschine_rund) {
				k = new Keks("r", teig);
			
		} else if(this instanceof Keksmaschine_mond) {
				k = new Keks("m", teig);
			
			
		} else if(this instanceof Keksmaschine_weihnachtsmann) {
				k = new Keks("w", teig);
		} 
		
		return k;
	}
}
