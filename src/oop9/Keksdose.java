package oop9;

import java.util.ArrayList;

public class Keksdose {
	private ArrayList<Keks> bestellung = new ArrayList<Keks>();
	

	// bestellung.size() > 0
	public void inhalt()
	{
		for(int i = 0; i < bestellung.size();i++)
		{
			if(bestellung.get(i) instanceof Doppelkeks)
			{
				
				System.out.println("Keks #" + (i+1) + ": Doppelkeks mit " + ((Doppelkeks)bestellung.get(i)).getFuellung() + "-Fuellung, " + bestellung.get(i).formToString() + "-Form und " + bestellung.get(i).teigToString());
			
			}else if(bestellung.get(i) instanceof Keks)
			{
				System.out.println("Keks #" + (i+1) + ": Einzelkeks mit " + ((Keks)bestellung.get(i)).formToString() + "-Form und " + ((Keks)bestellung.get(i)).teigToString());

			}
			
		}
	}
	
	// fuegt einen Keks der Keksdose hinzu, Keks != null
	
	public void fuelleDose(Keks k)
	{
		bestellung.add(k);
	}
}
