package oop9;

public class Doppelkeks extends Keks{
	private String fuellung; // Schokolade oder Marmelade
	
	public Doppelkeks(String form, String teig, String fuellung)
	{
		super(form,teig);
		this.fuellung = fuellung;
	}


	public String getFuellung() {
		return fuellung;
	}

	public void setFuellung(String fuellung) {
		this.fuellung = fuellung;
	}

}
