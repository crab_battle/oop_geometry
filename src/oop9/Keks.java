package oop9;

public class Keks{
	private String form; // r fuer rund; m fuer Mond, w fuer Weihnachtsmann
	private String teig; // m fuer Muerbteig, z fuer Zimtsternteig, s fuer Schokoladenteig
	
	public Keks(String form, String teig)
	{
		this.form = form;
		this.teig = teig;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getTeig() {
		return teig;
	}

	public void setTeig(String teig) {
		this.teig = teig;
	}
	

	// wandelt die codierte Form in ein Wort um, form = r, m oder w
	public String formToString() {
		if(form.equalsIgnoreCase("r")) {
			return "rund";
		} else if (form.equalsIgnoreCase("m")) {
			return "Mond";
		} else if (form.equalsIgnoreCase("w")) {
			return "Weihnachtsmann";
		} else {
			return null;	// nur wenn ungueltiger Keks gemacht wurde
		}
	}
	
	// wandelt den codierten Teig in ein Wort um, teig = m, z oder s
	public String teigToString() {
		if(teig.equalsIgnoreCase("m")) {
			return "Muerbteig";
		} else if (teig.equalsIgnoreCase("z")) {
			return "Zimtsternteig";
		} else if (teig.equalsIgnoreCase("s")) {
			return "Schokoladenteig";
		} else {
			return null;	// nur wenn ungueltiger Keks gemacht wurde
		}
	}
}
