package oop9;

public class Doppelkeksmaschine {
	// erzeugt einen Doppelkeks aus einem Keks mit der zugehoerigen Fuellung

	public Doppelkeks backe(Keks k, String fuellung)
	{
		return new Doppelkeks(k.getForm(), k.getTeig(), fuellung);
	}
}
