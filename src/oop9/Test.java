package oop9;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//3 Bestellungen werden erzeugt:
		Bestellung a1 = new Bestellung();
		a1.addPosition(3, "r", "m");
		a1.addPosition(3, "m", "z", "Schokolade");
		Bestellung a2 = new Bestellung();
		a2.addPosition(5, "w", "s");
		a2.addPosition(5, "r", "z");
		a2.addPosition(8, "w", "z", "Marmelade");
		Bestellung a3 = new Bestellung();
		a3.addPosition(1, "m", "z");
		a3.addPosition(4, "r", "m");
		a3.addPosition(5, "w", "m");
		a3.addPosition(5, "r", "s");
		
		//Bestellungen werden ausgegeben:
		System.out.println("Bestellung 1: ");
		a1.drucke();
		System.out.println("Bestellung 2: ");
		a2.drucke();
		System.out.println("Bestellung 3: ");
		a3.drucke();
		
		// Kekse werden erzeugt:
		Keksdose b1 = a1.abschliessen();
		Keksdose b2 = a2.abschliessen();
		Keksdose b3 = a3.abschliessen();
		
		// Kekse werden ausgegeben:
		System.out.println("Keksdose 1: ");
		b1.inhalt();
		System.out.println("Keksdose 2: ");
		b2.inhalt();
		System.out.println("Keksdose 3: ");
		b3.inhalt();

	}

}
