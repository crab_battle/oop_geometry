package oop9;

import java.util.ArrayList;

public class Bestellung {
	private ArrayList<Position> bestellungen;	// in Auftrag gegebene Bestellungen
	
	public Bestellung() {
		bestellungen = new ArrayList<Position>();
	}
	
	public void addPosition(int anzahl, String form, String teig)
	{
		bestellungen.add(new Position(anzahl, form, teig));
	}
	public void addPosition(int anzahl, String form, String teig, String fuellung)
	{
		bestellungen.add(new Position(anzahl, form, teig, fuellung));
	}
	
	// bestellungen.size() > 0
	public Keksdose abschliessen()
	{
		Keksdose kd = new Keksdose();
		Keksmaschine_mond mond = new Keksmaschine_mond();
		Keksmaschine_rund rund = new Keksmaschine_rund();
		Keksmaschine_weihnachtsmann weihnachtsmann = new Keksmaschine_weihnachtsmann();
		Doppelkeksmaschine doppel = new Doppelkeksmaschine();
		Keks result = null;
		for(int i = 0; i < bestellungen.size();i++)
		{
			Position p = bestellungen.get(i);
			for (int j = 0; j < p.getAnzahl(); j++) {
				
				if(p.getForm().equalsIgnoreCase("r")){
					result = rund.backe(p.getTeig());
				} else if(p.getForm().equalsIgnoreCase("m")) {
					result = mond.backe(p.getTeig());
				} else if(p.getForm().equalsIgnoreCase("w")) {
					result = weihnachtsmann.backe(p.getTeig());
				}
				if(p.getFuellung() != null) {
					kd.fuelleDose(doppel.backe(result, p.getFuellung()));
				} else {
					kd.fuelleDose(result);
				}
				
			}
			
		}
		return kd;
	}
	
	//bestellungen.size() > 0, sonst wird nichts gedruckt
	public void drucke()
	{
		for(int i = 0; i < bestellungen.size();i++)
		{
			Position p = bestellungen.get(i);
			if(p.getFuellung() == null)
			{
				System.out.println("Position #" + (i+1) + ":\tAnzahl: " + p.getAnzahl() + "\tForm: " + p.formToString() + "\tTeig: " + p.teigToString());
			}else
			{
				System.out.println("Position #" + (i+1) + ":\tAnzahl: " + p.getAnzahl() + "\tForm: " + p.formToString() + "\tTeig: " + p.teigToString() + "\tFuellung: " + p.getFuellung());
			}
			
		}
	}
}
