package oop7;

import java.security.InvalidParameterException;
import java.util.Random;

public class Auto extends Thread {
	//Himmelsrichtung wird als Integer gespeichert. Folgende Codierung:
	// 1 : Norden
	// 2 : Osten
	// -1: Sueden
	// -2: Westen
	private int himmelsrichtung;
	private char typ;	// B (beweglich) oder S (schnell)
	private Fahrbahn fahrbahn;
	private String bezeichnung;
	private String strategie;	//random (default), schlange oder kreis
	private int pos_x;	// >=0 && < Fahrbahn.breite
	private int pos_y;	// >=0 && < Fahrbahn.hoehe
	private int punkte = 0;	// <= 10
	private int geschwindigkeit; //Dauer des "Schlafens" in ms; bei typ S: default = 15, bei typ B: default = 25
	
	

	public Auto(char typ) throws InvalidParameterException {
		if(typ != 'B' && typ != 'S') {
			throw new InvalidParameterException("Ungueltiger Auto-Typ");
		}
		this.typ = typ;
		fahrbahn = Fahrbahn.getInstanz();
		strategie = "random";
		if(this.typ == 'S') {
			geschwindigkeit = 15;
		} else {
			geschwindigkeit = 25;
		}
	}
	
	public void setGeschwindigkeit(int k) {
		this.geschwindigkeit = k;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	public void setStrategie(String strategie) {
		if(strategie != "kreis" && strategie != "schlange" && strategie != "random") {
			throw new UnsupportedOperationException();	
		}
		this.strategie = strategie;
	}

	public String getStrategie() {
		return strategie;
	}

	public char getTyp()
	{
		return typ;
	}
	
	public int getPos_x() {
		return pos_x;
	}

	public int getPos_y() {
		return pos_y;
	}
	public int getPunkte() {
		return punkte;
	}
	
	public void erhoehePunkte() {
		punkte +=1;
	}
	public void erniedrigePunkte()
	{
		punkte -=1;
	}
	
	public void setPosition(int x, int y)
	{
		pos_x = x;
		pos_y = y;
	}
	
	public String getPosition()
	{
		return "x: " + pos_x + " y: " + pos_y;
	}
	
	public void setHimmelsrichtung(int h)
	{
		himmelsrichtung = h;
	}
	public int getHimmelsrichtung()
	{
		return himmelsrichtung;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
			String strategie = this.strategie;
			int rnd_number;
			while(!this.isInterrupted()) {
				if(strategie.equals("random")) {
				
					rnd_number = ((int)(Math.random()*10)) % 3;
			
					switch (rnd_number) {
					case 0:
						fahrbahn.bewegeAuto(this, -90);	
						break;
					case 1:
						fahrbahn.bewegeAuto(this, 0);
						break;
					case 2:
						fahrbahn.bewegeAuto(this, 90);
						break;
					default:
						break;
					}	
				} else if(strategie.equals("kreis")) {
					fahrbahn.bewegeAuto(this, 90);
							
				} else if(strategie.equals("schlange")) {
					int counter = 0;
					if(counter < 3) {
						fahrbahn.bewegeAuto(this, 0);
						counter++;
					} else if(counter >= 3 && counter < 5) {
						fahrbahn.bewegeAuto(this, 90);
						counter++;
					} else if (counter < 8) {
						fahrbahn.bewegeAuto(this, 0);
						counter++;
					} else if (counter >= 8 && counter < 10) {
						fahrbahn.bewegeAuto(this, -90);
						counter++;
					} else {
						counter = 0;
					}
				} else {
					// unreachable
				}
				try {
					if(this.typ == 'S')
						Thread.sleep(geschwindigkeit);
					else
						Thread.sleep(geschwindigkeit);
			
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					this.interrupt();
				
				}
			}
		
		}
		
	}

