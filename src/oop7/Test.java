package oop7;

import java.security.InvalidParameterException;

public class Test {
	
	public static void main(String[] args) {
		Fahrbahn fb = new Fahrbahn(5,5);
		try {
			Auto a1 = new Auto('S');
			fb.setzeAuto(a1, 1, 1);
			a1.setBezeichnung("Auto1");
			a1.setGeschwindigkeit(8);
			a1.start();
		} catch (InvalidParameterException e) {
			//Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		try{
			Auto a2 = new Auto('S');
			fb.setzeAuto(a2, 4, 1);
			a2.setBezeichnung("Auto2");
			a2.setGeschwindigkeit(5);
			a2.start();
		} catch (InvalidParameterException e) {
			//Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		Fahrbahn fb2 = new Fahrbahn(10,10);
		try {
			Auto a3 = new Auto('B');
			fb2.setzeAuto(a3, 9, 3);
			a3.setBezeichnung("Auto3");
			a3.start();
		} catch (InvalidParameterException e) {
			//Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		try {
			Auto a4 = new Auto('B');
			fb2.setzeAuto(a4, 1, 8);
			a4.setBezeichnung("Auto4");
			a4.setStrategie("schlange");
			a4.setGeschwindigkeit(10);
			a4.start();
		} catch (InvalidParameterException e) {
			//Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		try {
			Auto a5 = new Auto('S');
			fb2.setzeAuto(a5, 8, 5);
			a5.setBezeichnung("Auto5");
			a5.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		Fahrbahn fb3 = new Fahrbahn(12,12);
		try {
			Auto a6 = new Auto('S');
			fb3.setzeAuto(a6, 8, 5);
			a6.setBezeichnung("Auto6");
			a6.setGeschwindigkeit(12);
			a6.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		try {
			Auto a7 = new Auto('S');
			fb3.setzeAuto(a7, 5, 8);
			a7.setBezeichnung("Auto7");
			a7.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		try {
			Auto a8 = new Auto('B');
			fb3.setzeAuto(a8, 3, 2);
			a8.setBezeichnung("Auto8");
			a8.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		try {
			Auto a9 = new Auto('B');
			fb3.setzeAuto(a9, 8, 2);
			a9.setBezeichnung("Auto9");
			a9.setStrategie("schlange");
			a9.setGeschwindigkeit(20);
			a9.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
		
		try {
			Auto a10 = new Auto('S');
			fb3.setzeAuto(a10, 5, 5);
			a10.setBezeichnung("Auto10");
			a10.setStrategie("kreis");
			a10.start();
		} catch (InvalidParameterException e) {
			// Auto nimmt nicht teil
		} catch (UnsupportedOperationException e) {
			//Auto nimmt nicht teil
		}
	}
}
