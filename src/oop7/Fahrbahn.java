package oop7;

import java.security.InvalidParameterException;
import java.util.ArrayList;


public class Fahrbahn {
	private ArrayList<Auto> al;
	private int breite; // > 0
	private int hoehe;	// > 0
	
	private static Fahrbahn fahrbahn;
	
	public Fahrbahn()
	{
		this.breite = 10;
		this.hoehe = 10;
		al = new ArrayList<Auto>();
		Fahrbahn.fahrbahn = this;
		
	}
	
	public Fahrbahn(int breite, int hoehe){
		this.breite = breite;
		this.hoehe = hoehe;
		al = new ArrayList<Auto>();
		Fahrbahn.fahrbahn = this;
	}
	public boolean setzeAuto(Auto a, int x, int y)
	{
		if(x < breite && y < hoehe && x > 0 && y > 0)
		{
			a.setPosition(x, y);
			a.setHimmelsrichtung(1);
			al.add(a);
			return true;
		}else
		{
			throw new InvalidParameterException();
		}
	}
	

	
	public synchronized boolean bewegeAuto(Auto a, int rotation) 
	{
		int zahl = 0;
		int ziel_x = 0;
		int ziel_y = 0;
		int ziel_himmelsrichtung = 0;
		if(a.getTyp() == 'B')
		{
			zahl = 0;
		}else if(a.getTyp() == 'S') {
			zahl = 1;
		}
		switch (a.getHimmelsrichtung()) {
		//Norden
		case 1:
			switch (rotation) {
			case -90:
				ziel_x = a.getPos_x()-1;
				ziel_y = a.getPos_y()+zahl;
				ziel_himmelsrichtung = -2;
				break;
			case 0:
				ziel_x = a.getPos_x();
				ziel_y = a.getPos_y()+1;
				ziel_himmelsrichtung = 1;
				break;
			case 90:
				ziel_x = a.getPos_x()+1;
				ziel_y = a.getPos_y()+zahl;
				ziel_himmelsrichtung = 2;
				break;
			default:
				//Auto bleibt stehen
			}
			break;
		//Osten
		case 2:
			switch (rotation) {
			case -90:
				ziel_x = a.getPos_x()+zahl;
				ziel_y = a.getPos_y()+1;
				ziel_himmelsrichtung = 1;
				break;
			case 0:
				ziel_x = a.getPos_x()+1;
				ziel_y = a.getPos_y();
				ziel_himmelsrichtung = 2;
				break;
			case 90:
				ziel_x = a.getPos_x()+zahl;
				ziel_y = a.getPos_y()-1;
				ziel_himmelsrichtung = -1;
				break;
			default:
				//Auto bleibt stehen
			}
			break;
		//Sueden
		case -1:
			switch (rotation) {
			case -90:
				ziel_x = a.getPos_x()+1;
				ziel_y = a.getPos_y()-zahl;
				ziel_himmelsrichtung = 2;
				break;
			case 0:
				ziel_x = a.getPos_x();
				ziel_y = a.getPos_y()-1;
				ziel_himmelsrichtung = -1;
				break;
			case 90:
				ziel_x = a.getPos_x()-1;
				ziel_y = a.getPos_y()-zahl;
				ziel_himmelsrichtung = -2;
				break;
			default:
				//Auto bleibt stehen
			}
			break;
		//Westen
		case -2:
			switch (rotation) {
			case -90:
				ziel_x = a.getPos_x()-zahl;
				ziel_y = a.getPos_y()-1;
				ziel_himmelsrichtung = -1;
				break;
			case 0:
				ziel_x = a.getPos_x()-1;
				ziel_y = a.getPos_y();
				ziel_himmelsrichtung = -2;
				break;
			case 90:
				ziel_x = a.getPos_x()-zahl;
				ziel_y = a.getPos_y()+1;
				ziel_himmelsrichtung = 1;
				break;
			default:
				//Auto bleibt stehen
			}
			break;
		default:
			// Auto bleibt stehen
			break;
		}
		boolean pos_leer = true;
		
		//Ueberpruefung ob das Auto am Rand steht
		if(ziel_x < breite && ziel_y < hoehe && ziel_x >= 0 && ziel_y >= 0)
		{
			for(int i = 0; i < al.size();i++)
			{
				if(ziel_x == al.get(i).getPos_x() && ziel_y == al.get(i).getPos_y())
				{
					pos_leer = false;
					//Wenn sie frontal gegenueber stehen
					if(al.get(i).getHimmelsrichtung() + ziel_himmelsrichtung == 0)
					{
						
						//Fahrer bekommt Punkt, anderer bekommt keinen Punkt, Fahrer bleibt auf seiner Position
						a.erhoehePunkte();
					}
					//Falls das Auto von der Seite getroffen wird
					else{
						a.erhoehePunkte();
						al.get(i).erniedrigePunkte();
					}
						
				}
			}
			if(pos_leer)
			{
				
				a.setPosition(ziel_x, ziel_y);
				a.setHimmelsrichtung(ziel_himmelsrichtung);
			}
		
			}else
			{
				//Auto ist am Rand angekommen, wird um 180 Grad gedreht

				a.setHimmelsrichtung(-a.getHimmelsrichtung());
			}
		
		if(a.getPunkte() == 10)
		{
			System.out.println("Sieger ist " + a.getBezeichnung());
			
			beendeSpiel();
		}
		return true;
	}
	
	public synchronized void beendeSpiel()
	{
		for(int i = 0; i < al.size();i++)
		{
			al.get(i).interrupt();
		}
		System.out.println("Folgender Punktestand:");
		for (int i = 0; i < al.size(); i++) {
			System.out.println(al.get(i).getBezeichnung() + ": " + al.get(i).getPunkte());
		}
	}
	
	public synchronized static Fahrbahn getInstanz(){

		   if (fahrbahn == null)
			   fahrbahn = new Fahrbahn();

		   return fahrbahn;

	}
}
