package oop5;

import java.util.Iterator;



public class OrderedMap<E extends Shorter<? super E>,F> extends OrderedSet<E> {
	protected Node<E,F> head = null;
	
	public OrderedMap() {
		
	}
	
	
	public void insert(E e) {
	
		if(this.head == null) {
			head = new Node<E,F>(e,head,new Set<F>());
		} else {
			boolean inserted = false;
			Node<E,F> current = this.head;
			Node<E,F> next = current.getNext();
			while(!inserted) {
				if(current.getElem().shorter(e)) {
					if(next != null) {
						if(!next.getElem().shorter(e)) {
							current.setNext(new Node<E,F>(e,next,new Set<F>()));
							inserted = true;
						} else {
							current = next;
							next= next.getNext();
						}
					} else {
						current.setNext(new Node<E,F>(e,null,new Set<F>()));
						inserted = true;
					}
					
				
					// else nur im ersten Schleifendurchlauf relevant
				} else {
					this.head = new Node<E,F>(e,this.head,new Set<F>());
					inserted = true;
				}
			}
		}
		
	}
	
	public Iter<E> iterator() {
		return new Iter<E>(this.head);
	}

	protected class Node<E,F> {
		protected E elem;
		protected Node<E,F> next;
		protected Set<F> set;
		
		Node(E e, Node<E,F> next, Set<F> set) {
			this.elem = e;
			this.next = next;
			this.set = set;
		}
		
		Node<E,F> getNext() {
			return next;
		}
		
		E getElem() {
			return elem;
		}
		
		Set<F> getSet() {
			return set;
		}
		
		void setNext(Node<E,F> next) {
			this.next = next;
		}
	}
	
	
	protected class Iter<E> extends Set<E>.Iter<E> implements Iterator<E> {
		protected Node<E,F> first = null;
		protected Node<E,F> parent = null;
		protected Node<E,F> copy = null;
		protected int counter = 0;
		
		
		Iter(Node<E,F> first) {
			this.first = first;
			this.copy = first;
		}
		@Override
		public boolean hasNext() {
			return first != null;
		}

		@Override
		public E next() {
			if(first != null) {
				Node<E,F> result = first;
				first = first.getNext();
				counter++;
				if(counter == 2) {
					parent = copy;
				}
				if(counter > 2) {
					parent = parent.getNext();
				}
				return result.getElem();
			}
			return null;
			
		}

		@Override
		public void remove() {
			parent.setNext(first);
			
		}
		
		
		public Set<F>.Iter<F> iterator() {
			return first.set.iterator();
		}
		
		public Node<E,F> getFirst() {
			return this.first;
		}
	}
}
