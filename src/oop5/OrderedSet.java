package oop5;

public class OrderedSet<E extends Shorter<? super E>> extends Set<E> {
	
	@Override
	public void insert(E e) {
		if(this.head == null) {
			super.insert(e);
		} else {
			boolean inserted = false;
			Node<E> current = this.head;
			Node<E> next = current.getNext();
			while(!inserted) {
				if(current.getElem().shorter(e)) {
					if(next != null) {
						if(!next.getElem().shorter(e)) {
							current.setNext(new Node<E>(e,next));
							inserted = true;
						} else {
							current = next;
							next= next.getNext();
						}
					} else {
						current.setNext(new Node<E>(e,null));
						inserted = true;
					}
				
			
				// else nur im ersten Schleifendurchlauf relevant
				} else {
					this.head = new Node<E>(e,this.head);
					inserted = true;
				}
			}
		}
	}
	
}
