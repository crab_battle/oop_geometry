package oop5;

class Description implements  Shorter<Description> {
	
	String elem;
	
	public Description (String value)
	{
		this.elem = value;
	}

	@Override
	public boolean shorter(Description elem) {
		return elem.toString().length() > this.elem.length();
	}
	
	public int getRowCounts()
	{
		return elem.split("\n").length;
	}
	
	public String toString()
	{
		return elem;
	}
}
