package oop5;


import oop5.OrderedMap.Iter;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
			
		OrderedSet<Description> e2 = new OrderedSet<Description>();
		
		Description sc1 = new Description("333");
		Description sc2 = new Description("22");
		Description sc3 = new Description("1");
			e2.insert(sc1);
			e2.insert(sc2);
			e2.insert(sc3);
			
			OrderedSet<Description>.Iter<Description> iter2 = e2.iterator();
			while(iter2.hasNext()){
				System.out.println(iter2.next());
			
			}
		
		
		//ElapsedTime testing:
		ElapsedTime first = new ElapsedTime(5);
		ElapsedTime second = new ElapsedTime(7);
		
		System.out.println(first.count());
		System.out.println(first.shorter(second));
		System.out.println(first.count());
		
		
		//MeanElapsedTime testing
		
		MeanElapsedTime met1 = new MeanElapsedTime();
		met1.addTime(2);
		met1.addTime(3);
		met1.addTime(4);
		met1.addTime(3.5);
		met1.addTime(5.6);
		met1.addTime(5.4);
		
		MeanElapsedTime met2 = new MeanElapsedTime();
		met2.addTime(6.8);
		met2.addTime(0.3);
		met2.addTime(2);
		met2.addTime(3.9);
		met2.addTime(2.1);
		met2.addTime(3.9);
		
		MeanElapsedTime met3 = new MeanElapsedTime();
		met2.addTime(12.8);
		met2.addTime(4.3);
		met2.addTime(2);
		met2.addTime(54.9);
		met2.addTime(32.1);
		met2.addTime(32.9);
		
		MeanElapsedTime met4 = new MeanElapsedTime();
		met2.addTime(62.8);
		met2.addTime(1.3);
		met2.addTime(62);
		met2.addTime(37.9);
		met2.addTime(28.1);
		met2.addTime(39.9);
		
		System.out.println(met1.count());
		System.out.println(met1.shorter(met2));
		System.out.println(met1.count());
		System.out.println(met1.getMax());
		
		
		double[] cta = {3,2,1,4,1};
		CompositeTime ct = new CompositeTime(cta);
		
		double[] cta2 = {3,1,7,1000,9,777,53};
		CompositeTime ct2 = new CompositeTime(cta2);
		
		double[] cta3 = {15,30,55,1000,9,4,5,6,7,8,9};
		CompositeTime ct3 = new CompositeTime(cta3);
		
		System.out.println(ct.count());
		System.out.println(ct.shorter(ct2));
		
		System.out.println(ct.count());
		System.out.println(ct.getMin());
		

		//Testteil Punkt 2
		System.out.println("2. Testteil");
		OrderedMap<MeanElapsedTime,CompositeTime> map = new OrderedMap<MeanElapsedTime,CompositeTime>();
		map.insert(met1);
		map.insert(met2);
		OrderedMap<MeanElapsedTime,CompositeTime>.Iter<MeanElapsedTime> it1 = map.iterator();
		Set<CompositeTime>.Iter<CompositeTime> it2 = it1.iterator();
		it2.add(ct,it1.getFirst().getSet());
		it2.next();
		it2.add(ct2,it1.getFirst().getSet());
		it2.next();
		Set<CompositeTime>.Iter<CompositeTime> it2a = it1.iterator();
		while(it2a.hasNext()) {
			System.out.println(it2a.next().count());
		}
		
		it1.next();
		
		Set<CompositeTime>.Iter<CompositeTime> it3 = it1.iterator();
		it3.add(ct,it1.getFirst().getSet());
		it3.add(ct3,it1.getFirst().getSet());
		
		OrderedMap<MeanElapsedTime,CompositeTime>.Iter<MeanElapsedTime> it1a = map.iterator();
		
		//alle durchlaufen, minima und maxima ausgeben:
		while(it1a.hasNext()) {
			
			Set<CompositeTime>.Iter<CompositeTime> it2b = it1.iterator();
			while(it2b.hasNext()) {
				System.out.println("min: " + it2b.next().getMin());
			}
			System.out.println("max: " + it1a.next().getMax());
		}
		
		map.insert(met3);
		map.insert(met4);
		
		//Testteil Punkt 3
		System.out.println("3. Testteil: ");
		OrderedSet<MeanElapsedTime>.Iter<MeanElapsedTime> it45 = map.iterator();
		while(it45.hasNext()) {
			System.out.println(it45.next().getMax());
		}
		
		//Testteil Punkt 4
		System.out.println("4. Testteil: ");
		OrderedSet<ElapsedTime> finale = new OrderedSet<ElapsedTime>();
		OrderedMap<MeanElapsedTime,CompositeTime>.Iter<MeanElapsedTime> it55 = map.iterator();
		while(it55.hasNext()) {
			Set<CompositeTime>.Iter<CompositeTime> it2b = it1.iterator();
			while(it2b.hasNext()) {
				finale.insert(it2b.next());
			}
			finale.insert(it55.next());
		}
		
		OrderedSet<ElapsedTime>.Iter<ElapsedTime> it788 = finale.iterator();
		while(it788.hasNext()) {
			System.out.println(it788.next().count());
		}

/**
 * Erzeugen Sie eine Instanz von OrderedSet deren Elemente vom Typ Description sind.
 * Fuegen Sie einige Elemente in unsortierter Reihenfolge ein, lesen Sie alle Elemente
 * der Menge ueber den Iterator aus, und schreiben Sie die Anzahlen der Zeilen in die
 * Standard-Ausgabe. Fuehren Sie aenderungen durch und geben Sie die Elemente erneut aus.
 * Diesen Vorgang koennen Sie mit unterschiedlichen aenderungen so oft wiederholen,
 * wie es Ihnen als noetig erscheint.
 */
		System.out.println("OrderedSet mit Description");
		System.out.println("Ausgabe der Elemente: ");
		System.out.println("Anzahl der Elemente: ");
		System.out.println("Ausgabe der Elemente nach der Aenderungen: ");
		
		
/** 
 * Erzeugen Sie eine Instanz von OrderedMap, deren Elemente vom Typ MeanElapsedTime
 * sind und die auf Objekte vom Typ CompositeTime verweisen - nicht sehr sinnvoll,
 * aber gut zum Testen geeignet. Fuegen Sie einige Elemente und damit verbundene
 * Objekte ein, lesen Sie alles ueber die Iteratoren aus, und schreiben Sie jeweils
 * den groessten Messwert (fuer Elemente) bzw. die kuerzeste Einzelzeit (fuer Objekte,
 * auf die Elemente verweisen) in die Standard-Ausgabe. Testen Sie aenderungen aehnlich
 * wie bei Punkt 1.
 */
		System.out.println("OrderedMap mit MeanElapsedTime mit CompositeTime");
		System.out.println("Ausgabe der Elemente: ");
		System.out.println("Anzahl der Elemente: ");
		System.out.println("Ausgabe der Elemente nach der Aenderungen: ");
		
		
/**
 * Falls OrderedMap mit entsprechenden Typparameterersetzungen ein Untertyp von OrderedSet
 * ist, betrachten Sie die in Punkt 2 erzeugte Menge als Instanz von OrderedSet, fuegen
 * Sie noch einige Elemente ein, lesen Sie alle Elemente ueber den Iterator aus, und
 * schreiben Sie die groessten Messwerte in die Standard-Ausgabe. Falls OrderedMap kein
 * Untertyp von OrderedSet ist, geben Sie anstelle der Testergebnisse eine Begruendung
 * dafuer aus, warum zwischen diesen Typen keine Untertypbeziehung besteht.
 */
		System.out.println("");
		
		
		
/** 
 * Erzeugen Sie eine Instanz von OrderedSet, deren Elemente vom Typ ElapsedTime sind. Lesen
 * Sie alle Elemente der in Punkt 2 erzeugten (und moeglicherweise in Punkt 3 erweiterten)
 * Menge und alle Objekte, auf welche die Elemente verweisen, aus und fuegen Sie diese
 * (Instanzen von MeanElapsedTime ebenso wie von CompositeTime) in die neue Menge ein.
 * Lesen Sie alle Elemente der neuen Menge aus, und schreiben Sie die durch count
 * ermittelten Werte in die Standard-Ausgabe.
 */
		System.out.println("");
		
		

		
	}

}
