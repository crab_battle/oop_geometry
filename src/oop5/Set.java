package oop5;

import java.util.Iterator;


/*
 * Set enthaelt Nodes, die ein Element E sowie einen Verweis auf das naechste Listenelement enthalten
 */
public class Set<E> implements Iterable<E> {
	protected Node<E> head = null;	
	
	public Set() {
		
	}
	
	public void insert(E elem) {
		if(head == null) {
			head = new Node<E>(elem,head);
			return;
		} else {
			Iter<E> iterator = iterator();
			while(iterator.hasNext()) {
				
				if(iterator.next() == elem) {
					return;
				}
			}
			head = new Node<E>(elem,head);
		}
	}
	
	public Node<E> getHead() {
		return this.head;
	}
	
	
	public Iter<E> iterator() {
		return new Iter<E>(head);
	}
	
	public void setHead(Node<E> head) {
		this.head = head;
	}
	
	protected class Node<E> {
		protected E elem;			// Element E
		protected Node<E> next;	//Verweis auf das naechste Element
		
		Node(E e, Node<E> next) {
			this.elem = e;	//nicht null
			this.next = next;
		}
		
		Node<E> getNext() {
			return next;
		}
		
		E getElem() {
			return this.elem;
		}
		
		void setNext(Node<E> next) {
			this.next = next;
		}
		
	}
	
	protected class Iter<E> implements Iterator<E> {
		protected Node<E> first = null;
		protected Node<E> parent = null;
		protected Node<E> copy = null;
		protected int counter = 0;
		
		protected Iter() {
			
		}
		
		protected Iter(Node<E> first) {
			this.first = first;
			this.copy = first;
		}
		
		@Override
		public boolean hasNext() {
			return first != null;
		}

		@Override
		public E next() {
			if(first != null) {
				Node<E> result = first;
				first = first.getNext();
				counter++;
				if(counter == 2) {
					parent = copy;
				}
				if(counter > 2) {
					parent = parent.getNext();
				}
				return result.getElem();
			}
			return null;
		}

		@Override
		public void remove() {
			parent.setNext(first);
		}
		
		public void add(E e, Set<E> set) {
			set.insert(e);
		}
	}

}




