package oop5;

public class MeanElapsedTime extends ElapsedTime {

	private Set<Double> met = new Set<Double>();
	
	
	public MeanElapsedTime() {
		
	}
	
	public void addTime(double time)
	{
		met.insert(time);
	}
	
	private double getAverage()
	{
		double avg_sum = 0;
		int counter = 0;
		Set<Double>.Iter<Double> iter = met.iterator();
		while(iter.hasNext())
		{
			avg_sum += iter.next();
			counter ++;
			
		}
		return avg_sum / counter;
	}
	
	public double getMax()
	{
		double max = 0;
		double act = 0;
		Set<Double>.Iter<Double> iter = met.iterator();
		while(iter.hasNext())
		{
			act = iter.next();
			if(act > max)
				max = act;
			
		}
		return max;
	}
	
	public boolean shorter(MeanElapsedTime elem)
	{
		//Vor jedem shorter-Aufruf muss der Durchschnitt berechnet werden.
		//Duration von ElapsedTime wird als Durchschnitt verwendet.
		elem.setDuration(elem.getAverage());
		this.setDuration(this.getAverage());
		return super.shorter(elem);
	}

}
