package oop5;

public class ElapsedTime implements Shorter<ElapsedTime> {
	// LAUT ANGABE: GENAUE BEDEUTUNG VON UNTERTYPEN ABHAENGIG ???
	private int counter = 0;
	private double duration;
	
	public ElapsedTime(double duration)
	{
		this.duration = duration;
	}
	public ElapsedTime()
	{
		this.duration = 0;
	}
	public void setDuration(double duration)
	{
		this.duration = duration;
	}
	
	public int count() //ausgabe der anzahl der durchgefuehrten messungen
	{
		return this.counter;
	}
	
    public double getDuration()
    {
    	return this.duration;
    }
	
	//liefert true, wenn this (auf nicht naeher bestimmte Weise) kuerzer als das uebergebene Argument ist
	@Override
	public boolean shorter(ElapsedTime elem)
	{
		++counter;
		return duration < elem.getDuration();
	}
}
