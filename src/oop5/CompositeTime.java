package oop5;

public class CompositeTime extends ElapsedTime {
	double[] cta;
	
	public CompositeTime(double[] cta){
		this.cta = cta;
	}
	
	public int count(){
		return cta.length;
	}
	
	private double getSum(){
		int sum = 0;
		
		for(int i = 0; i<count(); i++){
			sum += cta[i];
		}
		
		return sum;
	}
	
	public boolean shorter(CompositeTime elem)
	{
		//Vor jedem shorter-Aufruf muss der Summe berechnet werden.
		//Duration von ElapsedTime wird als Summe verwendet.
		elem.setDuration(elem.getSum());
		this.setDuration(this.getSum());
		return super.shorter(elem);
	}
	
	public double getMin(){
		double min = cta[0];
		double act = 0;
		
		for(int i = 1; i < count(); i++){
			if(min > cta[i])
				min = cta[i];
			
		}
		return min;
	}
	
}
