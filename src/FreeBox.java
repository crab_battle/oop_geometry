
public class FreeBox implements Pict{
	
	String[] content;
	private double height;
	private double width;
	
	public FreeBox(String content)
	{
		this.content = content.split("\n");
		this.height = this.content.length;
		this.width = this.content[0].length();
	}
	
	public String toString()
	{
		
		String ret ="";
		//Zum Aufrunden
		double iheight = Math.ceil(height);
		double iwidth = Math.ceil(width);
		int act_width = 0;
		int act_height = 0;
		
		for (int i = 0; i < iheight; i++) {
			act_height = i;
			for(int j = 0; j < iwidth; j++){
				act_width = j;
				//Bei groesseren Seitenlaengen wird der Text so oft wie noetig neben- bzw. uebereinandergestellt
				if(act_height >= content.length)
					act_height -= content.length;
				
				if(act_width >= content[act_height].length())
					act_width -= content[act_height].length();
				
				ret+=content[act_height].charAt(act_width);
				
			}
			ret += "\n";
		}
		
		return ret;
	}

	@Override
	public void scale(double factor) {
		
		height *= factor;
		width *= factor;
		
	}
}
