
public class Test {
/*
 * Untertypbeziehung & Interface:
 * 						||     Pict     || 
 * 						 /      |        \  
 * 					    /       |         \      
 * 				|  Box  |  |Repeated<P>|  |FreeBox|
 * 		    	 /      \			\
 * 			    /        \			 \
 * 		 |ClearBox|     |DarkBox| 	|Scaled<P extends Pict>|
 * 
 * 
 * 
 */
	
	public static void main(String[] args) {
		//Test-Cases
		Box b1 = new Box(2, 2, 'O', '*');
		System.out.println(b1.toString());
		//Untertypbeziehung: FreeBox und ClearBox kann ohne Probleme zu Box umbenannt werden und wuerde ebenso funktionieren
		DarkBox db = new DarkBox(2, 2, 'x');
		System.out.println(db.toString());
		//Andere Moeglichkeit
		Box db1 = new DarkBox(2,2,'x');
		System.out.println(db1.toString());
		
		ClearBox cb = new ClearBox(2, 2);
		System.out.println(cb.toString());
		//Andere Moeglichkeit
		Box cb1 = new ClearBox(2, 2);
		System.out.println(cb1.toString());
		
		
		
		FreeBox fb = new FreeBox("1234\n5678");
		System.out.println(fb.toString());
		fb.scale(1.5);
		System.out.println(fb.toString());
		
		// Repeated<Box> wird erstellt
		Repeated<Box> aleks = new Repeated<Box>(3,3);
		aleks.add(new Box(9, 7, 'I', 'F'));
		aleks.add(new ClearBox(8, 4));
		aleks.add(new Box(3, 3, 'O', '*'));
		aleks.add(new ClearBox(6, 4));
		aleks.add(new Box(5, 5, 'X', '1'));
		aleks.add(new Box(4, 2, 'R', '-'));
		aleks.add(new ClearBox(2, 2));
		aleks.add(new Box(7, 6, 'W', '.'));
		aleks.add(new ClearBox(7, 3));
		
		//Ausgabe von Repeated ohne Skalierungsfaktor
		System.out.println(aleks.toString());
		
		//Repeated mit Skalierungsfaktor < 1
		aleks.scale(0.25);
		System.out.println(aleks.toString());

		//Repeated mit Skalierungsfaktor > 1
		aleks.scale(3);
		System.out.println(aleks.toString());
		
		// Repeated mit keinem Untertyp von Pict
		Repeated<Integer> xyz = new Repeated<Integer>(2,2);
		xyz.add(new Integer(213));
		xyz.add(new Integer(23155));
		xyz.add(new Integer(324));
		xyz.add(new Integer(1));
		
		System.out.println(xyz.toString());
		
		// Repeated mit char von Pict
		Repeated<Character> abc = new Repeated<Character>(3,2);
		abc.add(new Character('a'));
		abc.add(new Character('R'));
		abc.add(new Character('O'));
		abc.add(new Character('F'));
		abc.add(new Character('i'));
		abc.add(new Character('q'));
		
		System.out.println(abc.toString());
		
		
		//Scaled
		System.out.println("Scaled");
		Scaled<Box> grid = new Scaled<Box>(3,3);
		
		grid.add(new ClearBox(4, 5));
		grid.add(new Box(4, 7, 'O', 'P'));
		grid.add(new ClearBox(6, 8));
		
		grid.add(new Box(6, 6, 'X', 'O'));
		grid.add(new FreeBox("Keinr\nmagZu\nsichr\nungen"));
		grid.add(new Box(8, 5, 'O', 'K'));
		
		grid.add(new ClearBox(4, 6));
		grid.add(new Box(8, 8, 'O', 'H'));
		grid.add(new Box(3, 2, '1', '2'));
		
		//Ausgabe von Scaled ohne Skalierung
		System.out.println(grid.toString());
		
		//Scaled mit Skalierung < 1
		grid.scale(0.5);
		System.out.println(grid.toString());

		//Scaled mit Skalierung > 1
		grid.scale(2);
		System.out.println(grid.toString());
		
		
	}
	
}
