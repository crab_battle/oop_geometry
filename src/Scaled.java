public class Scaled<P extends Pict> extends Repeated<Pict> implements Pict{

	public Scaled(int height, int width) {
		super(height, width);
	}
	
	
	//public String toSting() wird von superclass repeated geerbt und setzt vorraus, dass Variable scaleFactor = 1.0
	
	
	public void scale(double factor){
		for(int i = 0; i<getHeight(); i++){
			for(int j = 0; j<getWidth(); j++){
				getElem(i,j).scale(factor);
			}
		}
	}
	
}
