
public class DarkBox extends Box{
	//Eigene Variable da es veraenderbar sein muss und deshalb nicht final sein darf.
	private char char_both;
	
	public DarkBox(double height, double width, char char_both){
		super(height, width,char_both);
		this.char_both = char_both;
	}
	
	@Override
	public String toString() {
		String ret = "";
		
		//Zum Aufrunden
		double iheight = Math.ceil(super.getHeight());
		double iwidth = Math.ceil(super.getWidth());
		
		
		for (int i = 0; i < iheight; i++) {
			for(int j = 0; j < iwidth; j++){
					ret += char_both;
			}
			ret += "\n";
		}
		
		return ret;
	}

	public char getChar() {
		return char_both;
	}

	public void setChar(char char_both) {
		this.char_both = char_both;
	}
}
