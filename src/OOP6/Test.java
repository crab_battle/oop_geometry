package OOP6;

import java.util.Iterator;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AndroidList al = new AndroidList();
		
		//Instanzen aller Skins werden erzeugt:
		Skin gs = new GepanzerterSkin();
		Skin bs = new BeruehrungssensitiverSkin();
		Skin hs = new HochfesterSkin();
		
		//Instanzen aller Software-Levels werden erzeugt
		Software sw1 = new SW_level1();
		Software sw2 = new SW_level2();
		Software sw3 = new SW_level3();
		Software sw4= new SW_level4();
		Software sw5= new SW_level5();
		
		SensorenAktoren sa1 = new SensorenAktoren("Kit1",10);
		SensorenAktoren sa2 = new SensorenAktoren("Kit1",1);
		
		//Hilfskraft darf nur beruehrungssensitiven Skin haben, deshalb Ausgabe Fehler
		Android hilfskraft = new Hilfskraft();
		hilfskraft.setSoftware(sw1);
		hilfskraft.setSensorenAktoren(sa2);
		hilfskraft.setSkin(gs);
		System.out.println(al.insert(hilfskraft));
		//Hilfskraft mit Beruehrungssensitiver Skin ist erlaubt, jedoch falsche Software
		hilfskraft.setSoftware(sw5);
		hilfskraft.setSkin(bs);
		System.out.println(al.insert(hilfskraft));
		//Hilfskraft mit richtiger Skin und Software, jedoch zu starken sensorenaktoren
		hilfskraft.setSoftware(sw1);
		hilfskraft.setSensorenAktoren(sa1);
		System.out.println(al.insert(hilfskraft));
		//Hilfskraft mit komplett richtigen Werten, kein Fehler wird ausgegeben
		hilfskraft.setSensorenAktoren(sa2);
		System.out.println(al.insert(hilfskraft));
		
		
		Android kaempfer = new Kaempfer();

		kaempfer.setSoftware(sw5);
		kaempfer.setSensorenAktoren(sa1);
		kaempfer.setSkin(gs);
		System.out.println(al.insert(kaempfer));
		
		//finden und ausgeben der in der Liste befindlichen Androiden
		System.out.println(al.find(1));
		System.out.println(al.find(2));
		System.out.println(al.find(3));
		
		//Instanzen aller restlichen Androide werden erzeugt:
		Android bauarbeiter = new Bauarbeiter();
		Android gesellschafter = new Gesellschafter();
		Android leibwaechter = new Leibwaechter();
		Android objektbewacher = new Objektbewacher();
		Android servicetechniker = new Servicetechniker();
		Android transportarbeiter = new Transportarbeiter();
		
		//einige Androide werden ausgestattet:
		bauarbeiter.setSkin(hs);
		bauarbeiter.setSoftware(sw3);
		bauarbeiter.setSensorenAktoren(new SensorenAktoren("Kit2",2));
		
		gesellschafter.setSkin(new BeruehrungssensitiverSkin());
		gesellschafter.setSoftware(sw2);
		gesellschafter.setSensorenAktoren(new SensorenAktoren("Kit3",1));
		
		leibwaechter.setSkin(new GepanzerterSkin());
		leibwaechter.setSoftware(sw4);
		leibwaechter.setSensorenAktoren(new SensorenAktoren("Kit5",7));
		
		objektbewacher.setSkin(new GepanzerterSkin());
		objektbewacher.setSoftware(new SW_level4());
		objektbewacher.setSensorenAktoren(new SensorenAktoren("Kit6",5));
		
		System.out.println(al.insert(bauarbeiter));
		//Fehler beim einfuegen des Gesellschafter wegen falscher Software
		System.out.println(al.insert(gesellschafter));
		System.out.println(al.insert(leibwaechter));
		System.out.println(al.insert(objektbewacher));
		
		// alle Elemente der Androidliste werden ausgelesen
		Iterator<Android> it = al.iterator();
		for(int i=1; it.hasNext(); i++) {
			Android x = it.next();
			System.out.println(i + " Skin: " + x.getSkin().toString() + 
								" Software: " + x.getSw().toString() + 
								" SensorenAktorenKit: " + x.getSa().toString());
		}
		
		
		//neue Ausstattung fuer Android leibwaechter
		leibwaechter.setSensorenAktoren(new SensorenAktoren("Kit 555",1));
		al.insert(leibwaechter);
		leibwaechter.setSkin(new HochfesterSkin());
		al.insert(leibwaechter);
		
		Iterator<Android> it22 = al.iterator();
		
		System.out.println("\n Android unter Nummer 4 hat neuen Skin sowie neue Sensorenaktoren:");
		
		// AndroidListe mit einem veraenderten Androiden wird ausgegeben
		for(int i=1; it22.hasNext(); i++) {
			Android x = it22.next();
			System.out.println(i + " Skin: " + x.getSkin().toString() + 
								" Software: " + x.getSw().toString() + 
								" SensorenAktorenKit: " + x.getSa().toString());
		}
		
		//die Versionen des Androiden leibwaechter werden ausgegeben
		Iterator<Android> it2 = leibwaechter.getVersionen().iterator();
		System.out.println("\n alle Aenderungen des Androiden Leibwaechter werden angezeigt:");
		while(it2.hasNext()) {
			Android x = it2.next();
			System.out.println(" Skin: " + x.getSkin().toString() + 
					" Software: " + x.getSw().toString() + 
					" SensorenAktorenKit: " + x.getSa().toString());
		}
		
		
		
		
		
	}

}
