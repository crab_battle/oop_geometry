package OOP6;

public class SW_level3 extends Software {
	
	public SW_level3()
	{
		super.maxkw = 5;
		super.level = "3";
	}
	
	public Software checkHilfskraft(Android a){
		return null;
	}
	
	public Software checkGesellschafter(Android a){
		return null;
	}
	
	public Software checkBauarbeiter(Android a){
		super.typ = "Bauarbeitersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkServicetechniker(Android a){
		super.typ = "Servicetechnikersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkTransportarbeiter(Android a){
		super.typ = "Transportarbeitersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkObjektbewacher(Android a){
		return null;
	}
	
	public Software checkLeibwaechter(Android a){
		return null;
	}
	
	public Software checkKaempfer(Android a){
		return null;
	}
	
}
