package OOP6;

public class SW_level4 extends Software {

	public SW_level4()
	{
		super.maxkw = 10;
		super.level = "4";
	}
	
	public Software checkHilfskraft(Android a){
		return null;
	}
	
	public Software checkGesellschafter(Android a){
		return null;
	}
	
	public Software checkBauarbeiter(Android a){
		super.typ = "Bauarbeitersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkServicetechniker(Android a){
		super.typ = "Servicetechnikersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkTransportarbeiter(Android a){
		super.typ = "Transportarbeitersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkObjektbewacher(Android a){
		super.typ = "Objektbewachersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkLeibwaechter(Android a){
		super.typ = "Leibwaechtersoftware";		
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkKaempfer(Android a){
		return null;
	}
	
}
