package OOP6;

public abstract class Skin {
	protected int sn; 	//darf nicht mehr veraendert werden nachdem gesetzt
	
	public Skin(){
		
	}
	public int getSn()
	{
		return sn;
	}
	
	public abstract Skin checkBeschuetzer(Android a);
	public abstract Skin checkSchwerarbeiter(Android a);
	public abstract Skin checkBediener(Android a);
	
	
}
