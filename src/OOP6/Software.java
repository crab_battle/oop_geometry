package OOP6;

public abstract class Software {
	protected int sn;	//darf nicht mehr veraendert werden nachdem gesetzt
	protected String typ;
	protected String level;
	protected Integer maxkw;
	
	public abstract Software checkHilfskraft(Android a);
	
	public abstract Software checkGesellschafter(Android a);	
	
	public abstract Software checkBauarbeiter(Android a);
	
	public abstract Software checkServicetechniker(Android a);
	
	public abstract Software checkTransportarbeiter(Android a);
	
	public abstract Software checkObjektbewacher(Android a);
	
	public abstract Software checkLeibwaechter(Android a);
	
	public abstract Software checkKaempfer(Android a);
	
	public String toString()
	{
		return typ + " Level: " + level;
	}
	
}
