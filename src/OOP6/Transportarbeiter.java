package OOP6;

public class Transportarbeiter extends Schwerarbeiter {
	
	public Transportarbeiter() {
		
	}
	
	public Transportarbeiter(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Transportarbeiter b = new Transportarbeiter(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkTransportarbeiter(this);
	}

}
