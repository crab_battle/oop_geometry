package OOP6;

public class Kaempfer extends Beschuetzer {
	
	public Kaempfer() {
		
	}
	
	public Kaempfer(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Kaempfer b = new Kaempfer(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkKaempfer(this);
	}
}
