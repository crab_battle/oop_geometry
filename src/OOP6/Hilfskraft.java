package OOP6;

public class Hilfskraft extends Bediener {

	public Hilfskraft() {
		
	}	
	
	public Hilfskraft(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Hilfskraft b = new Hilfskraft(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkHilfskraft(this);
	}

}
