package OOP6;

public class Gesellschafter extends Bediener {
	
	public Gesellschafter() {
		
	}
	
	public Gesellschafter(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Gesellschafter b = new Gesellschafter(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkGesellschafter(this);
	}

}
