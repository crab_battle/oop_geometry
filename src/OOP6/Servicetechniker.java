package OOP6;

public class Servicetechniker extends Schwerarbeiter {
	
	public Servicetechniker() {
		
	}
	
	public Servicetechniker(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Servicetechniker b = new Servicetechniker(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkServicetechniker(this);
	}

}
