package OOP6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class AndroidList extends ArrayList<Android> {
	HashMap<Integer,Android> hm = new HashMap<Integer,Android>();
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	// fuegt nur einen Androiden in die Liste ein, wenn er richtig ausgestattet ist
	public String insert(Android a)
	{
		if(a.getSkin() != null && a.getSa() != null && a.getSw() != null) {
			
			Android b = hm.put(a.getSn(), a);	// referenziert die alte Version des Androiden, falls existent
			if(b == null) {
				super.add(a);
				a.getVersionen().add(a.copy(a));
			} else {
				super.add(super.indexOf(b),a);
				a.getVersionen().add(a.copy(a));
				super.remove(b);
			}
			
			
		} else {
			return "Fehler beim einfuegen!";
		}
		return "";
		
	}
	
	// falls der Androide in der Liste enthalten ist, wird seine Ausstattung ausgegeben, sonst null
	public String find(int sn)
	{
		if(hm.get(sn) != null) {
			return "Skin: " + hm.get(sn).getSkin().toString() +
				" Software: " + hm.get(sn).getSw().toString() +
				" SensorenAktorenKit: " + hm.get(sn).getSa().toString();
		}
		return null;
	}
	
	// Methode der Superklasse
	public Iterator<Android> iterator()
	{
		return super.iterator();
	}
	
	
}
