package OOP6;

public class Bauarbeiter extends Schwerarbeiter {
	
	public Bauarbeiter() {
		
	}
	
	public Bauarbeiter(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Bauarbeiter b = new Bauarbeiter(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkBauarbeiter(this);
	}
	
}
