package OOP6;

import java.util.HashMap;

public class SensorenAktoren {
	private int sn;
	private String bezeichnung;
	private int kw;
	
	public SensorenAktoren()
	{
		
	}
	public SensorenAktoren(String bezeichnung, int kw)
	{
		this.bezeichnung = bezeichnung;
		this.kw = kw;
	}
	
	// wird nur gesetzt, wenn die hoehe der kw passend zur Software ist
	public SensorenAktoren setSensorenAktoren(Android a, Software s){
		HashMap<Integer,SensorenAktoren> hm = new HashMap<Integer,SensorenAktoren>();
		if(s != null)
		{
			if(s.maxkw != null)
			{
				for (int i = 0; i < s.maxkw; i++) {
					hm.put(i, this);
				}
				sn = a.getSn();
				
				return hm.put(kw-1, this);	
			}
			return this;
		}
		return null;
	}
	
	public void setBezeichnung(String bezeichnung){
		this.bezeichnung = bezeichnung;
	}
	
	public void setKw(int kw){
		this.kw = kw;
	}
	
	public String toString()
	{
		return bezeichnung;
	}
	public int getSn()
	{
		return sn;
	}
	
	public int getKw() {
		return kw;
	}
	
	public String getBezeichnung() {
		return bezeichnung;
	}
}
