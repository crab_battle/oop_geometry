package OOP6;

public class SW_level5 extends Software {
	
	public SW_level5()
	{
		super.maxkw = null;
		super.level = "5";
	}
	
	public Software checkHilfskraft(Android a){
		return null;
	}
	
	public Software checkGesellschafter(Android a){
		return null;
	}
	
	public Software checkBauarbeiter(Android a){
		return null;
	}
	
	public Software checkServicetechniker(Android a){
		return null;
	}
	
	public Software checkTransportarbeiter(Android a){
		return null;
	}
	
	public Software checkObjektbewacher(Android a){
		return null;
	}
	
	public Software checkLeibwaechter(Android a){
		return null;
	}
	
	public Software checkKaempfer(Android a){
		super.typ = "Kaempfersoftware";
		super.sn = a.getSn();

		return this;
	}
	
}
