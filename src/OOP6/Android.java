package OOP6;
import java.util.ArrayList;

public abstract class Android {
	protected Skin s;
	protected final int sn;
	protected Software sw;
	protected SensorenAktoren sa;
	protected ArrayList<Android> versionen;	//alle Versionen, die in die AndroidList eingefuegt werden, werden hier gespeichert

	public static int sn_counter = 1;	//Seriennummer wird fortlaufend erhoeht 
	
	public Android() {
		sn = sn_counter++;
		versionen = new ArrayList<Android>();
	}
	
	//Kopierkonstruktor
	public Android(Android a) {
		this.sn = a.getSn();
	}
	
	public Skin getSkin(){
		return s;
	}
	
	public Software getSw() {
		return sw;
	}
	
	public SensorenAktoren getSa()	{
		return sa;
	}
	
	public int getSn() {
		return sn;
	}
	
	// nur passende Sensorenaktoren (kw nicht hoeher als erlaubt) werden gesetzt, bei unpassenden Sensorenaktoren wirds sa null gesetzt 
	public void setSensorenAktoren(SensorenAktoren sa) {
		this.sa = sa.setSensorenAktoren(this, sw);
	}
	
	public ArrayList<Android> getVersionen() {
		return versionen;
	}
	
	//liefert eine Kopie eines Androiden
	public abstract Android copy(Android a);
	
	// nur passende Skins werden gesetzt, bei unpassenden Skins wird s null gesetzt
	public abstract void setSkin(Skin s);
	
	// nur passende Software wird gesetzt, bei unpassender Software wird sw null gesetzt
	public abstract void setSoftware(Software s);
}
