package OOP6;

public class Objektbewacher extends Beschuetzer {
	
	public Objektbewacher() {
		
	}
	
	public Objektbewacher(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Objektbewacher b = new Objektbewacher(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkObjektbewacher(this);
	}

}
