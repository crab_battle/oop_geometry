package OOP6;

public class Leibwaechter extends Beschuetzer {
	
	public Leibwaechter() {
		
	}
	
	public Leibwaechter(Android a) {
		// TODO Auto-generated constructor stub
	}


	public Android copy(Android a) {
		Leibwaechter b = new Leibwaechter(a);
		b.setSkin(a.getSkin());
		b.setSoftware(a.getSw());
		b.setSensorenAktoren(a.getSa());
		return b;
	}
	
	public void setSoftware(Software s){
		super.sw = s.checkLeibwaechter(this);
	}
}
