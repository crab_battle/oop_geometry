package OOP6;

public class SW_level1 extends Software {
	
	public SW_level1 ()
	{
		super.maxkw = 1;
		super.level = "1";
	}
	
	public Software checkHilfskraft(Android a){
		super.typ = "Hilfskraftsoftware";
		super.sn = a.getSn();
		
		return this;
	}
	
	public Software checkGesellschafter(Android a){
		super.typ = "Gesellschaftersoftware";
		super.sn = a.getSn();

		return this;
	}
	
	public Software checkBauarbeiter(Android a){
		return null;
	}
	
	public Software checkServicetechniker(Android a){
		return null;
	}
	
	public Software checkTransportarbeiter(Android a){
		return null;
	}
	
	public Software checkObjektbewacher(Android a){
		return null;
	}
	
	public Software checkLeibwaechter(Android a){
		return null;
	}
	
	public Software checkKaempfer(Android a){
		return null;
	}
	
}
