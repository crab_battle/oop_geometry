package oop8;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Iterator;

@Author(name = "Aleksandar Kamenica")
public class Test {
	
	@Author(name = "Aleksandar Kamenica")
	public static void main(String[] args) {
		
		//Bauernhof und Traktoren erstellt:
		Bauernhof ab = new Bauernhof("Bauernhof1");
		Bauernhof cd = new Bauernhof("Bauernhof2");
		Bauernhof ausgewaehlt; // hier wird immer derjenige Bauernhof referenziert, den man gerade ansprechen will ueber get()
		Dieseltraktor x = new Dieseltraktor(3);
		x.setAnbau(new Drillmaschine(3));
		x.erhoeheVerbrauch(3);
		x.erhoeheVerbrauch(5);
		x.addBetriebsstunden(3);
		x.addBetriebsstunden(8);
		
		Dieseltraktor y = new Dieseltraktor(5);
		y.setAnbau(new Drillmaschine(5));
		y.erhoeheVerbrauch(1);
		y.addBetriebsstunden(1);
		Biogastraktor z = new Biogastraktor(4.8);
		z.setAnbau(new Duengerstreuer(3.5));
		z.erhoeheVerbrauch(12);
		z.addBetriebsstunden(12);
		Biogastraktor q = new Biogastraktor(4.3);
	    q.setAnbau(new Duengerstreuer(2));
		q.erhoeheVerbrauch(9);
		q.addBetriebsstunden(9);
		
		Set bauernhoefe = new Set();
		bauernhoefe.insert(ab);
		bauernhoefe.insert(cd);
		
		//Traktoren den Bauernhoefen hinzugefuegt
		ausgewaehlt = (Bauernhof)bauernhoefe.get("Bauernhof1");
		ausgewaehlt.addTraktor(x);
		ausgewaehlt.addTraktor(y);
		ausgewaehlt.addTraktor(z);
		ausgewaehlt = (Bauernhof)bauernhoefe.get("Bauernhof2");
		ausgewaehlt.addTraktor(q);
		
		// Traktor 1 entfernen:
		ausgewaehlt = (Bauernhof)bauernhoefe.get("Bauernhof1");
		ausgewaehlt.removeTraktor(1);
		
		//alle Bauernhoefe und Nummern der Traktoren ausgeben:
		Iterator it1 = bauernhoefe.iterator();
		while(it1.hasNext()) {
			Bauernhof a = (Bauernhof) it1.next();
			System.out.println(a.getName() + ":");
			Iterator it2 = a.getTraktorenmenge().iterator();
			while(it2.hasNext()) {
				Traktor curr = (Traktor)it2.next();
				System.out.println(curr.getSn());
			}
		}
		
		System.out.println(ausgewaehlt.avgBetriebsstundenArt());
		
		
		System.out.println(ausgewaehlt.avgBetriebsstunden());
		System.out.println(ausgewaehlt.avgDieselverbrauch());
		System.out.println(ausgewaehlt.avgGasverbrauch());
		
		
		System.out.println("\n==================Reflections==================");
		formatedReflection.outputReflection(Anbau.class);
		formatedReflection.outputReflection(Author.class);
		formatedReflection.outputReflection(Bauernhof.class);
		formatedReflection.outputReflection(Biogastraktor.class);
		formatedReflection.outputReflection(Dieseltraktor.class);
		formatedReflection.outputReflection(Drillmaschine.class);
		formatedReflection.outputReflection(Duengerstreuer.class);
		formatedReflection.outputReflection(formatedReflection.class);
		formatedReflection.outputReflection(Set.class);
		formatedReflection.outputReflection(Test.class);
		formatedReflection.outputReflection(Traktor.class);
		
	}

	
}
