package oop8;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.TYPE,
		java.lang.annotation.ElementType.METHOD,
		java.lang.annotation.ElementType.CONSTRUCTOR})
@Author(name = "Anton Blazicevic")
public @interface Author {

	@Author(name = "Aleksandar Kamenica")
	String name();
	
}
