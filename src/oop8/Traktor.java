package oop8;

@Author(name = "Anton Blazicevic")
public abstract class Traktor {
	private static int sn_counter = 0;
	private final int sn; //unveraenderliche seriennummer ueber die der traktor angesprochen wird, wird durch den Konstruktor gesetzt
	private int betriebsstunden;
	private Object verbrauch = 0;
	private Anbau anbau;

	@Author(name = "Anton Blazicevic")
	public Traktor(Object verbrauch)
	{
		this.verbrauch = verbrauch;
		this.sn = sn_counter++;
		this.betriebsstunden = 0;
	}
	
	@Author(name = "Deniz Gezgin")
	public void setVerbrauch(Object verbrauch) {
		this.verbrauch = verbrauch;
	}
	
	@Author(name = "Deniz Gezgin")
	public Object getVerbrauch() {
		return this.verbrauch;
	}
	
	@Author(name = "Anton Blazicevic")
	public int getSn()
	{
		return this.sn;
	}
	
	@Author(name = "Deniz Gezgin")
	public void setAnbau(Anbau a)
	{
		this.anbau = a;
	}
	
	@Author(name = "Deniz Gezgin")
	public Anbau getAnbau()
	{
		return this.anbau;
	}
	
	@Author(name = "Anton Blazicevic")
	public int getBetriebsstunden()
	{
		return this.betriebsstunden;
	}
	
	@Author(name = "Anton Blazicevic")
	public void addBetriebsstunden(int betriebsstunden)
	{
		this.betriebsstunden += betriebsstunden;
	}
	
	@Author(name = "Anton Blazicevic")
	public void changeAnbau(Anbau neu)
	{
		this.anbau = neu;
	}
}
