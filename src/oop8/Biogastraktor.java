package oop8;

@Author(name = "Aleksandar Kamenica")
public class Biogastraktor extends Traktor {
	
	@Author(name = "Deniz Gezgin")
	public Biogastraktor(double verbrauch) {
		super(verbrauch);
	}
	
	@Author(name = "Deniz Gezgin")
	public void erhoeheVerbrauch(double verbrauch) {

		super.setVerbrauch((Double)super.getVerbrauch() + verbrauch);

	}
	

}
