package oop8;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
 
@Author (name = "Anton Blazicevic")
public class formatedReflection 
{
	
	/**
	 * gibt eine formatierte uebersicht ueber alle klassen, konstruktoren, methoden und die gruppenmitglieder (autoren)
	 * @param theClass eine klasse aus der alle annotationen geladen werden
	 */
	
	@Author (name = "Anton Blazicevic")
	public static void outputReflection(Class theClass)
	{
		//Klassenname
		System.out.println("\n\nclass: \n\t" + theClass.getName() + " ");
		
		//Klassenannotation
		if(theClass.isAnnotationPresent(Author.class))
		{
			System.out.println("\t by " + ((Author) theClass.getAnnotation(Author.class)).name());
		}
		
		System.out.println("constructor(s):");
		
		for(Constructor con: theClass.getDeclaredConstructors())
		{
			//Konstruktorannotation
			if(con.isAnnotationPresent(Author.class))
			{
				//Konstruktorname
				System.out.println("\t" + con);
				System.out.println("\t by " + ((Author) con.getAnnotation(Author.class)).name());
			}
		}
		
		System.out.println("method(s):");
		
		for(Method m: theClass.getDeclaredMethods())
		{
			//Methodenname
			System.out.println("\t" + m);
			
			//Methodenannotation
			if(m.isAnnotationPresent(Author.class))
			{
				System.out.println("\t by " + m.getAnnotation(Author.class).name());
			}
		}
		
	}
	
}
