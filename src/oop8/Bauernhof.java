package oop8;

import java.util.Iterator;

@Author(name = "Aleksandar Kamenica")
public class Bauernhof {
	private final String name;	// unveraenderlicher Name, Bauernhof wird ueber Namen angesprochen
	private Set traktoren;	// die Menge von Traktoren eines Bauernhofs
	
	@Author(name = "Aleksandar Kamenica")
	public Bauernhof(String name) {
		this.name = name;
		this.traktoren = new Set();
	}
	
	@Author(name = "Aleksandar Kamenica")
	public String getName() {
		return name;
	}
	
	@Author(name = "Aleksandar Kamenica")
	public void addTraktor(Traktor t) {
		traktoren.insert(t);
	}
	
	@Author(name = "Aleksandar Kamenica")
	public void removeTraktor(int sn) {
		traktoren.remove(sn);
	}
	
	@Author(name = "Aleksandar Kamenica")
	public Set getTraktorenmenge() {
		return this.traktoren;
	}
	
	// gibt die durchschnittlichen Betriebsstunden aufgeschluesselt nach dem Antrieb der Traktoren
	@Author(name = "Aleksandar Kamenica")
	public String avgBetriebsstundenArt() {
		String result = "";
		Iterator it_diesel = getDieselTraktoren(traktoren).iterator();
		Iterator it_biogas = getBiogasTraktoren(traktoren).iterator();
		double diesel = 0;
		int dieselcounter = 0;
		double biogas = 0;
		int biogascounter = 0;
		Traktor t;
		while(it_diesel.hasNext()) {
			t = (Traktor)it_diesel.next();
			
			diesel += t.getBetriebsstunden();
			dieselcounter++;
		}
		
		while(it_biogas.hasNext()) {
			t = (Traktor)it_biogas.next();
			
			biogas += t.getBetriebsstunden();
			biogascounter++;
		}
		
		result += "Durchschnitt der Betriebsstunden der Dieseltraktoren: ";
		if(dieselcounter == 0) {
			result += dieselcounter;
		} else {
			result += diesel / dieselcounter;
		}
		
		result += " \nDurchschnitt der Betriebsstunden der Biogastraktoren: ";
		if(biogascounter == 0) {
			result += biogascounter;
		} else {
			result += biogas / biogascounter;
		}
		return result;
	}
	
	@Author(name="Deniz Gezgin")
	public String avgBetriebsstunden()
	{
		String result = "";
		Iterator it_saen = getDrillmaschineTraktoren(traktoren).iterator();
		Iterator it_duengen = getDuengerTraktoren(traktoren).iterator();
		Iterator it_all = traktoren.iterator();
		double saen = 0;
		int saencounter = 0;
		double duengen = 0;
		int duengencounter = 0;
		double traktoren = 0;
		int traktorencounter = 0;
		Traktor t;
		
		while(it_all.hasNext()) {
			t = (Traktor)it_all.next();
			
			traktoren += t.getBetriebsstunden();
			traktorencounter++;
		}
		
		while(it_saen.hasNext()) {
			t = (Traktor)it_saen.next();
			
			saen += t.getBetriebsstunden();
			saencounter++;
		}
		
		while(it_duengen.hasNext()) {
			t = (Traktor)it_duengen.next();
			
			duengen += t.getBetriebsstunden();
			duengencounter++;
		}
		
		result += "Durchschnitt der Betriebsstunden aller Traktoren: ";
		if(traktorencounter == 0) {
			result += traktorencounter;
		} else {
			result += traktoren / traktorencounter;
		}
		
		result += "\nDurchschnitt der Betriebsstunden der Drillmaschinen (Saen): ";
		if(saencounter == 0) {
			result += saencounter;
		} else {
			result += saen / saencounter;
		}
		
		result += " \nDurchschnitt der Betriebsstunden der Duengerstreuer (Duengen): ";
		if(duengencounter == 0) {
			result += duengencounter;
		} else {
			result += duengen / duengencounter;
		}
		return result;
	}
	
	
	@Author(name="Deniz Gezgin")
	public String avgDieselverbrauch()
	{
		String result = "";
		Iterator it_diesel = getDieselTraktoren(traktoren).iterator();
		Iterator it_saen = getDrillmaschineTraktoren(getDieselTraktoren(traktoren)).iterator();
		Iterator it_duengen = getDuengerTraktoren(getDieselTraktoren(traktoren)).iterator();
		double saen = 0;
		int saencounter = 0;
		double duengen = 0;
		int duengencounter = 0;
		double diesel = 0;
		int dieselcounter = 0;
		Traktor t;
		
		while(it_diesel.hasNext()) {
			t = (Traktor)it_diesel.next();
			
			diesel += (Integer)t.getVerbrauch();
			dieselcounter++;
		}
		
		while(it_saen.hasNext()) {
			t = (Traktor)it_saen.next();
			

			saen += (Integer)t.getVerbrauch();

			saencounter++;
		}
		
		while(it_duengen.hasNext()) {
			t = (Traktor)it_duengen.next();
			

			duengen += (Integer)t.getVerbrauch();

			duengencounter++;
		}
		
		result += "Durchschnittsverbrauch aller Dieseltraktoren: ";
		if(dieselcounter == 0) {
			result += dieselcounter;
		} else {
			result += diesel / dieselcounter;
		}
		
		result += "\nDurchschnittsverbrauch aller Dieseltraktoren mit Drillmaschinen (Saen): ";
		if(saencounter == 0) {
			result += saencounter;
		} else {
			result += saen / saencounter;
		}
		
		result += " \nDurchschnittsverbrauch aller Dieseltraktoren mit Duengerstreuer (Duengen): ";
		if(duengencounter == 0) {
			result += duengencounter;
		} else {
			result += duengen / duengencounter;
		}
		return result;
	}
	
	@Author(name="Deniz Gezgin")
	public String avgGasverbrauch()
	{
		String result = "";
		Iterator it_biogas = getBiogasTraktoren(traktoren).iterator();
		Iterator it_saen = getDrillmaschineTraktoren(getBiogasTraktoren(traktoren)).iterator();
		Iterator it_duengen = getDuengerTraktoren(getBiogasTraktoren(traktoren)).iterator();
		double saen = 0;
		int saencounter = 0;
		double duengen = 0;
		int duengencounter = 0;
		double biogas = 0;
		int biogascounter = 0;
		Traktor t;
		
		while(it_biogas.hasNext()) {
			t = (Traktor)it_biogas.next();

			biogas += (Double)t.getVerbrauch();

			biogascounter++;
		}
		
		while(it_saen.hasNext()) {
			t = (Traktor)it_saen.next();
			
			saen += (Double)t.getVerbrauch();

			saencounter++;
		}
		
		while(it_duengen.hasNext()) {
			t = (Traktor)it_duengen.next();
			
			duengen += (Double)t.getVerbrauch();

			duengencounter++;
		}
		
		result += "Durchschnittsverbrauch der Betriebsstunden aller Traktoren: ";
		if(biogascounter == 0) {
			result += biogascounter;
		} else {
			result += biogas / biogascounter;
		}
		
		result += "\nDurchschnittsverbrauch der Betriebsstunden der Drillmaschinen (Saen): ";
		if(saencounter == 0) {
			result += saencounter;
		} else {
			result += saen / saencounter;
		}
		
		result += " \nDurchschnittsverbrauch der Betriebsstunden der Duengerstreuer (Duengen): ";
		if(duengencounter == 0) {
			result += duengencounter;
		} else {
			result += duengen / duengencounter;
		}
		return result;
	}
	
	
	@Author(name="Deniz Gezgin")
	private Set getBiogasTraktoren(Set trak)
	{
		Iterator it = trak.iterator();
		Set return_set = new Set();
		while(it.hasNext()) {
			Object x = it.next();
			if(x instanceof Biogastraktor) {
				return_set.insert(x);
			} 
		}
		
		return return_set;
	}
	
	@Author(name="Deniz Gezgin")
	private Set getDieselTraktoren(Set trak)
	{
		Iterator it = trak.iterator();
		Set return_set = new Set();
		while(it.hasNext()) {
			Object x = it.next();
			if(x instanceof Dieseltraktor) {
				return_set.insert(x);
			} 
		}
		
		return return_set;
	}
	
	@Author(name="Deniz Gezgin")
	private Set getDuengerTraktoren(Set trak)
	{
		Iterator it = trak.iterator();
		Set return_set = new Set();
		while(it.hasNext()) {
			Traktor x = (Traktor)it.next();
			if(x.getAnbau() instanceof Duengerstreuer) {
				return_set.insert(x);
			} 
		}
		
		return return_set;
	}
	
	@Author(name="Deniz Gezgin")
	private Set getDrillmaschineTraktoren(Set trak)
	{
		Iterator it = trak.iterator();
		Set return_set = new Set();
		while(it.hasNext()) {
			Traktor x = (Traktor)it.next();
			if(x.getAnbau() instanceof Drillmaschine) {
				return_set.insert(x);
			} 
		}
		
		return return_set;
	}
	
}
