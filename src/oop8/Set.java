package oop8;


import java.util.Iterator;
/*
 * ein Set mit einigen Methoden angepasst fuer Objekte von Bauernhof und Traktor (z.B. remove und get)
 */
@Author(name = "Aleksandar Kamenica")
public class Set implements Iterable {
	protected Node head = null;	
	
	@Author(name = "Aleksandar Kamenica")
	public Set() {
		
	}
	
	@Author(name = "Aleksandar Kamenica")
	public void insert(Object elem) {
		if(head == null) {
			head = new Node (elem,head);
			return;
		} else {
			head = new Node(elem,head);
		}
	}
	
	//Object entweder String oder int
	@Author(name = "Aleksandar Kamenica")
	public void remove(Object o) {
		Iter it = new Iter(head);
		while(it.hasNext()) {
			Object x = it.next();
			
			//bei einer Menge von Bauernhoefen
			if(o instanceof String) {
				if(x instanceof Bauernhof) {
					Bauernhof y = (Bauernhof)x;
					if(y.getName().equals((String)o)) {
						it.remove();
					}
				}
				
			//bei einer Menge von Traktoren
			} else if(o instanceof Integer) {
				if(x instanceof Traktor) {
					if(((Traktor) x).getSn() == (Integer)o) {
						it.remove();
					}
				}
			}
		}
	}
	
	//Object entweder String oder int
	@Author(name = "Aleksandar Kamenica")
	public Object get(Object o) {
		Iter it = new Iter(head);
		while(it.hasNext()) {
			Object x = it.next();
			
			//bei einer Menge von Bauernhoefen:
			if(o instanceof String) {
				if(x instanceof Bauernhof) {
					Bauernhof y = (Bauernhof)x;
					if(y.getName().equals((String)o)) {
						return x;
					}
				}
			
			//bei einer Menge von Traktoren:
			} else if(o instanceof Integer) {
				if(x instanceof Traktor) {
					Traktor y = (Traktor)x;
					if(y.getSn() == (Integer)o) {
						return x;
					}
				}
			}
		}
		return null;
	}
	
	
	
	@Author(name = "Aleksandar Kamenica")
	public Iter iterator() {
		return new Iter(head);
	}
	
	@Author(name = "Aleksandar Kamenica")
	public void setHead(Node head) {
		this.head = head;
	}
	
	@Author(name = "Aleksandar Kamenica")
	private class Node {
		private Object elem;			// Element 
		private Node next;	//Verweis auf das naechste Element
		
		@Author(name = "Aleksandar Kamenica")
		private Node(Object e, Node next) {
			this.elem = e;	//nicht null
			this.next = next;
		}
		
		@Author(name = "Aleksandar Kamenica")
		public Node getNext() {
			return next;
		}
		
		@Author(name = "Aleksandar Kamenica")
		public Object getElem() {
			return this.elem;
		}
		
		@Author(name = "Aleksandar Kamenica")
		public void setNext(Node next) {
			this.next = next;
		}
		
	}
	
	@Author(name = "Aleksandar Kamenica")
	private class Iter implements Iterator {
		private Node first = null;
		private Node parent = null;
		private Node copy = null;
		private int counter = 0;
		
		@Author(name = "Aleksandar Kamenica")
		private Iter() {
			
		}
		
		@Author(name = "Aleksandar Kamenica")
		private Iter(Node first) {
			this.first = first;
			this.copy = first;
		}
		
		@Override
		@Author(name = "Aleksandar Kamenica")
		public boolean hasNext() {
			return first != null;
		}

		@Override
		@Author(name = "Aleksandar Kamenica")
		public Object next() {
			if(first != null) {
				Node result = first;
				first = first.getNext();
				counter++;
				if(counter == 2) {
					parent = copy;
				}
				if(counter > 2) {
					parent = parent.getNext();
				}
				return result.getElem();
			}
			return null;
		}

		@Override
		@Author(name = "Aleksandar Kamenica")
		public void remove() {
			parent.setNext(first);
		}
		
	}

}