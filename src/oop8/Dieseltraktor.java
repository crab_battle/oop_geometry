package oop8;

@Author(name = "Deniz Gezgin")
public class Dieseltraktor extends Traktor {
	
	@Author(name = "Deniz Gezgin")
	public Dieseltraktor(int verbrauch)
	{
		super(verbrauch);
	}
	
	@Author(name = "Deniz Gezgin")
	public void erhoeheVerbrauch(int verbrauch) {

		super.setVerbrauch((Integer)super.getVerbrauch() + verbrauch);

	}
	
	
}
